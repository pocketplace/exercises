using System;
using System.IO;
using System.Collections.Generic;
class EvenAndOdd
{
    #region ReadWrite
    List<object> output = new List<object>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        string outputValue = string.Empty;

        for (int i = 0; i < output.Count; i++)
        {
            outputValue += output[i] + " ";
        }

        sw.Write(outputValue.Trim());
        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        EvenAndOdd problem = new EvenAndOdd();
        problem.Start();
    }
    #endregion

    private List<object> ResolveProblem()
    {
        List<object> result = new List<object>();
        List<object> even = new List<object>();
        List<object> odd = new List<object>();

        int count = int.Parse(NextToken());

        for (int i = 0; i < count; i++)
        {
            int value = int.Parse(NextToken());
            if (value % 2 == 0)
                even.Add(value);
            else
                odd.Add(value);
        }

        result.AddRange(odd);
        result.AddRange(even);

        return result;
    }
}