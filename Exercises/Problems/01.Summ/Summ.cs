﻿using System;
using System.IO;
class Summ
{
    string[] input = new string[2];
    int output;

    string inputFileName = "input.txt";
    string outputFileName = "output.txt";

    string path = Directory.GetCurrentDirectory();

    private void ReadFile(string fileName)
    {
        StreamReader sr = new StreamReader(fileName);//path + "\\" + inputFileName);
        string tmp = sr.ReadLine();
        sr.Close();

        input = tmp.Split(Char.Parse(" "));
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);
        sw.WriteLine(output);
        sw.Close();
    }

    private int[] ParseInput()
    {
        int[] result = new int[input.Length];

        for (int i = 0; i < input.Length; i++)
        {
            int value;
            if (int.TryParse(input[i], out value))
                result[i] = value;
        }

        return result;
    }

    private bool ValidateInput(int[] input)
    {
        bool validate = true;

        for (int i = 0; i < input.Length; i++)
        {
            if (input[i] < -100 || input[i] > 100)
            {
                validate = false;
                break;
            }
        }

        return validate;
    }

    private int SummFromInput(int[] input)
    {
        int result = 0;
        for (int i = 0; i < input.Length; i++)
        {
            result += input[i];
        }

        return result;
    }

    public void Start()
    {
        ReadFile(path + "\\" + inputFileName);
        int[] tmp = ParseInput();

        if (!ValidateInput(tmp))
        {
            Console.WriteLine("wrong input values");
            return;
        }

        output = SummFromInput(tmp);

        WriteToFile(path + "\\" + outputFileName);
    }

    static void Main()
    {
        Summ summ = new Summ();
        summ.Start();
    }
}