using System;
using System.IO;
using System.Collections.Generic;
class Iosif
{
    #region ReadWrite
    List<int> output = new List<int>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        string outputValue = string.Empty;

        for (int i = 0; i < output.Count; i++)
        {
            outputValue += output[i] + " ";
        }

        sw.Write(outputValue.Trim());
        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        Iosif problem = new Iosif();
        problem.Start();
    }
    #endregion

    private List<int> ResolveProblem()
    {
        List<int> result = new List<int>();

        int count = int.Parse(NextToken());
        int period = int.Parse(NextToken());

        List<int> people = new List<int>();

        for (int i = 0; i < count; i++)
            people.Add(1);

        int indexOfAlive = GetLastAlive(people, period);

        result.Add(indexOfAlive + 1);

        return result;
    }

    private int GetLastAlive(List<int> people, int order)
    {
        int indexOfAlive = -1;
        int tmpIndex = 0;
        int tmpOrder = order;

        while (true)
        {
            if (!people.Contains(1))
                break;

            if (people[tmpIndex] == 1)
            {
                if (tmpOrder == 1)
                {
                    people[tmpIndex] = 0;
                    tmpOrder = order;
                    indexOfAlive = tmpIndex;
                }
                else
                    tmpOrder--;
            }

            tmpIndex++;

            if (tmpIndex == people.Count)
                tmpIndex = 0;
        }

        return indexOfAlive;
    }
}