using System;
using System.IO;
using System.Collections.Generic;
class SameNumbers
{
    #region ReadWrite
    List<object> output = new List<object>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        string outputValue = string.Empty;

        for (int i = 0; i < output.Count; i++)
        {
            outputValue += output[i] + " ";
        }

        sw.Write(outputValue.Trim());
        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        SameNumbers problem = new SameNumbers();
        problem.Start();
    }
    #endregion

    private List<object> ResolveProblem()
    {
        List<object> result = new List<object>();
        List<int> values = new List<int>();

        int count = int.Parse(NextToken());

        int firstPosition = 0;
        int secondPosition = 0;
        int sameNumber = 0;

        for (int i = 0; i < count; i++)
        {
            int number = int.Parse(NextToken());
            if (values.Contains(number))
            {
                secondPosition = i + 1;
                sameNumber = number;
                break;
            }
            else
                values.Add(number);
        }

        if (secondPosition != 0)
        {
            for (int i = 0; i < values.Count; i++)
            {
                if (values[i] == sameNumber)
                {
                    firstPosition = i + 1;
                    break;
                }

            }
        }

        result.Add(firstPosition);
        result.Add(secondPosition);

        return result;
    }
}