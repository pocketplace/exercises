using System;
using System.IO;
using System.Collections.Generic;
class SummOfDigits
{
    #region ReadWrite
    List<object> output = new List<object>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        string outputValue = string.Empty;

        for (int i = 0; i < output.Count; i++)
        {
            outputValue += output[i] + " ";
        }

        sw.WriteLine(outputValue);
        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        SummOfDigits problem = new SummOfDigits();
        problem.Start();
    }
    #endregion

    private List<object> ResolveProblem()
    {
        List<object> result = new List<object>();

        string number = NextToken();
        int sum = 0;

        for (int i = 0; i < number.Length; i++)
        {
            int tmp = 0;
            if (int.TryParse(number[i].ToString(), out tmp))
                sum += tmp;
        }
        result.Add(sum);

        return result;
    }
}