using System;
using System.IO;
using System.Collections.Generic;
class ShortBiggestDivider
{
    #region ReadWrite
    List<object> output = new List<object>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        string outputValue = string.Empty;

        for (int i = 0; i < output.Count; i++)
        {
            outputValue += output[i] + " ";
        }

        sw.Write(outputValue.Trim());
        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        ShortBiggestDivider problem = new ShortBiggestDivider();
        problem.Start();
    }
    #endregion

    private List<object> ResolveProblem()
    {
        List<object> result = new List<object>();

        int first = int.Parse(NextToken());
        int second = int.Parse(NextToken());

        int minNumber = first <= second ? first : second;
        int maxNumber = first == minNumber ? second : first;

        if (minNumber < maxNumber && maxNumber % minNumber == 0)
        {
            result.Add(minNumber);
            return result;
        }    

        int maxCycleValue = (int)Math.Sqrt(minNumber);

        int maxDivider = 1;

        for (int div = 2; div <= maxCycleValue; div++)
        {
            if (minNumber % div == 0)
            {
                int pairDivider = minNumber / div;

                if (maxNumber % pairDivider == 0 && maxDivider < pairDivider)
                {
                    maxDivider = pairDivider;
                }
                else if (maxNumber % div == 0 && maxDivider < div)
                {
                    maxDivider = div;
                }
            }
        }

        result.Add(maxDivider);

        return result;
    }
}