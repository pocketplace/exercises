using System;
using System.IO;
using System.Collections.Generic;
class PowerOfTwo
{
    #region ReadWrite
    private int firstRow = 0;
    List<int> input = new List<int>();
    List<object> output = new List<object>();

    int lineIndex = 0;

    string inputFileName = "input.txt";
    string outputFileName = "output.txt";

    private void ReadFile(string fileName)
    {
        StreamReader sr = new StreamReader(fileName);

        string tmp;

        while (!string.IsNullOrEmpty(tmp = sr.ReadLine()))
        {
            TokenReadLine(tmp);
            ResolveProblem();
        }

        sr.Close();
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        for (int i = 0; i < output.Count; i++)
        {
            sw.WriteLine(output[i] + " ");
        }
        sw.Close();
    }

    private void TokenReadLine(string line)
    {
        string token = string.Empty;
        bool readToken = !char.IsWhiteSpace(line[0]);

        while (lineIndex < line.Length)
        {
            if (readToken)
            {
                if (char.IsWhiteSpace(line[lineIndex]))
                {
                    readToken = false;
                    input.Add(int.Parse(token));
                    token = string.Empty;
                }
                else
                {
                    token += line[lineIndex];
                }
            }
            else
            {
                if (!char.IsWhiteSpace(line[lineIndex]))
                {
                    readToken = true;
                    token += line[lineIndex];
                }
                else
                {
                    lineIndex++;
                    continue;
                }
            }

            lineIndex++;
        }

        lineIndex = 0;

        // �� �������� ��� ���������� �������� � ���� �������� ����� ������
        if (readToken)
        {
            input.Add(int.Parse(token));
        }
    }
    #endregion

    #region Main
    public void Start()
    {
        ReadFile(inputFileName);

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        PowerOfTwo problem = new PowerOfTwo();
        problem.Start();
    }
    #endregion

    private void ResolveProblem()
    {
        int value = input[0];
        bool result = false;

        int tmp;

        for (int i = 0; (tmp = (int)Math.Pow(2, i)) <= value; i++)
        {
            if (tmp == value)
            {
                result = true;
                break;
            }
        }

        string outputValue = result ? "YES" : "NO";
        output.Add(outputValue);
    }
}