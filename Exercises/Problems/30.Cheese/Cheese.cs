using System;
using System.IO;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Reflection;

class Cheese
{
    #region ReadWrite
    List<int> output = new List<int>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        string outputValue = string.Empty;

        for (int i = 0; i < output.Count; i++)
        {
            if (i % 2 == 0)
            {
                outputValue += output[i] + " ";
            }
            else
            {
                outputValue += output[i] + "\n";
            }
        }

        sw.Write(outputValue.Trim());
        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        Cheese problem = new Cheese();
        problem.Start();
    }
    #endregion

    int x;
    int y;
    bool[,] cheese;
    int[] startCell;

    private List<int> ResolveProblem()
    {
        List<int> result = new List<int>();

        x = int.Parse(NextToken());
        y = int.Parse(NextToken());

        Random random = new Random();

        int startX = random.Next(0, x);
        int startY = random.Next(0, y);

        // ���������� �������������� ������
        // ��� ������ �������������� � ��������� ������� ������ ���������� ��������� ��� ���� ��� ������ �� ��������� ������
        // ���� ����� ��������� ��������, �� ����� ����������. ������ - ��)
        if (x % 2 != 0 && y % 2 != 0)
        {
            while ((startX + startY) % 2 != 0)
            {
                startX = random.Next(0, x);
                startY = random.Next(0, y);
            }
        }

        // ������� ������ ����
        cheese = new bool[x, y];


        // �������� ��������� ������
        int[] startPosition = { startX, startY };
        List<string> startDirection = new List<string> { "stop" };
        // ���������� �� ����������, ��� ��� ��� ����� ���� �� 3 ������� ������� �� ����� ��������� �� �������
        startCell = startPosition;

        // � �������� �� �����������
        FillNextCell(startPosition, startDirection, ref result);

        // �������� � ����� ����������� ������ ������ ���
        List<string> directions = GetClosestDirections(startX, startY);

        // ���� ��� ������ ���, ��������� ������� �� �� ���� ����, � ��������� ����������� ���������, �� ��������� � ����� ������� �������
        if (directions.Count > 1 && 
            !(startX == 0 || startX == (x - 1) || startY == 0 || startY == (y - 1)))
        {
            List<string> preferedDirections = new List<string>();

            if (x >= y)
            {
                preferedDirections.Add("down");
                preferedDirections.Add("up");
            }
            else
            {
                preferedDirections.Add("left");
                preferedDirections.Add("right");
            }

            foreach (var dir in directions)
            {
                if (preferedDirections.Contains(dir))
                {
                    directions.Clear();
                    directions.Add(dir);
                    break;
                }
            }
        }


        for (int i = 0; i < x * y; i++)
        {
            startPosition = FillNextCell(startPosition, directions, ref result);

            directions = GetClosestDirections(startPosition[0], startPosition[1]);

            if (directions[0] == "stop")
                break;
        }

        // ������� ��������, ��� ��� � ������ ��������� ���������� � 1
        for (int i = 0; i < result.Count; i++)
            result[i]++;

        return result;
    }
    /// <summary>
    /// ���������� ����������� ������� ���������, � - ����� �������, � - ����� �����
    /// </summary>
    /// <returns></returns>
    private List<string> GetClosestDirections(int startX, int startY)
    {
        Dictionary<string, int> directionToDistance = new Dictionary<string, int>();

        int distance = 0;
        List<string> result = new List<string>();

        // �������� ��� 4 ����������� �� �������� ������ � ������� ����
        
        // �����
        for (int i = startX - 1; i >= -1; i--)
        {
            if ( i == -1 || cheese[i, startY])
            {
                directionToDistance.Add("left", distance);
                distance = 0;
                break;
            }

            distance++;
        }

        // ������
        for (int i = startX + 1; i <= x; i++)
        {
            if (i == x || cheese[i, startY])
            {
                directionToDistance.Add("right", distance);
                distance = 0;
                break;
            }

            distance++;
        }

        // �����
        for (int j = startY - 1; j >= -1; j--)
        {
            if (j == -1 || cheese[startX, j])
            {
                directionToDistance.Add("down", distance);
                distance = 0;
                break;
            }

            distance++;
        }

        // ������
        for (int j = startY + 1; j <= y; j++)
        {
            if (j == y || cheese[startX, j])
            {
                directionToDistance.Add("up", distance);
                distance = 0;
                break;
            }

            distance++;
        }


        // ������� ������� ������� ����������
        foreach (var kvp in directionToDistance)
        {
            if (kvp.Value == 0)
                directionToDistance.Remove(kvp.Key);
        }


        // ���� �� �������� �� ������� ����������, ������ ���� ������ ������, ���� �����. ������ ��� ������ ������
        if (directionToDistance.Count == 0)
        {
            result.Add("stop");
            return result;
        }

        // ����� ������� ����� ������� �� �� ������� ����������
        int minDistance = x + y;
        List<int> distances = new List<int>();

        foreach (var kvp in directionToDistance)
            distances.Add(kvp.Value);

        distances.Sort();
        minDistance = distances[0];

        // ����� ������� �� ������� ��� ��������, � ������� ���������� ������ ������������
        foreach (var kvp in directionToDistance)
        {
            if (kvp.Value > minDistance)
                directionToDistance.Remove(kvp.Key);
        }

        // ���������� ���������� �����������
        foreach (var kvp in directionToDistance)
            result.Add(kvp.Key);

        return result;
    }

    private int[] FillNextCell(int[] startPosition, List<string> directions, ref List<int> result)
    {
        // ������ ���������� ��������� ������ � ����������� �� �����������
        int[] directionCoordinates = { 0, 0 };

        // ������� ������ ��������� ������ ���� ����������� ���������
        List<int[]> directionCells = new List<int[]>(directions.Count);

        // ��� ������� �� ��������� ����������� ������ ����������
        for (int i = 0; i < directions.Count; i++)
        {
            switch (directions[i])
            {
                case "left":
                    directionCoordinates[0]--;
                    break;
                case "right":
                    directionCoordinates[0]++;
                    break;
                case "down":
                    directionCoordinates[1]--;
                    break;
                case "up":
                    directionCoordinates[1]++;
                    break;
                case "stop":
                    break;
            }

            directionCells.Add(directionCoordinates);
            
            directionCoordinates[0] = 0;
            directionCoordinates[1] = 0;
        }


        // ��������� 1 - ����
        // ��������� 2 - ������ � ����������� �������
        if (directionCells.Count > 1)
        {
            // �� ������ ���� ����� �������� ������ 'directionCells' ��������� ������ �������� ���� �� ���� �� ��������� ������
            directionCoordinates = directionCells[0];

            for (int i = directionCells.Count - 1; i >= 0 ; i--)
            {
                if (IsCorner(directionCells[i]))
                {
                    directionCoordinates = directionCells[i];
                    directionCells.Clear();
                    directionCells.Add(directionCoordinates);
                    break;
                }

                if (!HasFilledOrtogonalNeighbour(directionCells[i]))
                {
                    directionCells.RemoveAt(i);
                }
            }
        }

        if (directionCells.Count > 0)
            directionCoordinates = directionCells[0];


        int[] newPosition = { startPosition[0] + directionCoordinates[0], startPosition[1] + directionCoordinates[1] };

        cheese[newPosition[0], newPosition[1]] = true;
        result.AddRange(newPosition);

        return newPosition;
    }

    private bool IsCorner(int[] position)
    {
        return (position[0] == 0 && position[1] == 0 ||
            position[0] == (x - 1) && position[1] == 0 ||
            position[0] == 0 && position[1] == (y - 1) ||
            position[0] == (x - 1) && position[1] == (y - 1));
    }

    /// <summary>
    /// ����� ���������� ��� ������ ����������� ��������, ���� ��������� ����������� ���������.
    /// ����������� ��� ������������� ������, ��� ���� ������� ���� ����� - ���������� ����������� ������ - ��� ����.
    /// ������� ���������� true ���� ���������� ����������� ������� ������ ������.
    /// </summary>
    /// <param name="cell">����������� ������</param>
    /// <returns></returns>
    private bool HasFilledOrtogonalNeighbour(int[] cell)
    {
        int filledNeighboursCount = 0;
        int[] neighbourCell = cell;

        if (--neighbourCell[0] >= 0)
        {
            if (cheese[neighbourCell[0], neighbourCell[1]])
            {
                filledNeighboursCount++;
                neighbourCell = cell;
            } 
        }

        if (++neighbourCell[0] < x)
        {
            if (cheese[neighbourCell[0], neighbourCell[1]])
            {
                filledNeighboursCount++;
                neighbourCell = cell;
            }
        }

        if (--neighbourCell[1] >= 0)
        {
            if (cheese[neighbourCell[0], neighbourCell[1]])
            {
                filledNeighboursCount++;
                neighbourCell = cell;
            }
        }

        if (++neighbourCell[1] < y)
        {
            if (cheese[neighbourCell[0], neighbourCell[1]])
            {
                filledNeighboursCount++;
                neighbourCell = cell;
            }
        }

        return filledNeighboursCount > 1;
    }
}