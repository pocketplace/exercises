using System;
using System.IO;
using System.Collections.Generic;
class Sequence3
{
    #region ReadWrite
    List<int> output = new List<int>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName, true);

        string outputValue = string.Empty;

        for (int i = 0; i < output.Count; i++)
        {
            outputValue += output[i] + " ";
        }

        sw.Write(outputValue.Trim());
        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        Sequence3 problem = new Sequence3();
        problem.Start();
    }
    #endregion

    private List<int> ResolveProblem()
    {
        List<int> result = new List<int>();

        int count = 0;

        while (true)
        {
            int charsCount = int.Parse(NextToken());
            if (charsCount == 0)
                break;

            int number = 0;
            for (int i = 0; i < charsCount; i++)
            {
                number *= 10;
                number += int.Parse(NextToken());
            }

            result.Add(number);
            count++;
        }

        StreamWriter sw = new StreamWriter(outputFileName);

        sw.WriteLine(count);
        sw.Close();

        return result;
    }
}