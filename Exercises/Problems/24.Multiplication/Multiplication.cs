using System;
using System.IO;
using System.Collections.Generic;
class Multiplication
{
    #region ReadWrite
    List<int> output = new List<int>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        string outputValue = string.Empty;

        for (int i = 0; i < output.Count; i++)
        {
            outputValue += output[i] + " ";
        }

        sw.Write(outputValue.Trim());
        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        Multiplication problem = new Multiplication();
        problem.Start();
    }
    #endregion

    private List<int> ResolveProblem()
    {
        int zeroIndex = -1;

        // max positive values (far zero)
        Values positiveMax = new Values(true, true);
        // min posotive values (near zero)
        Values positiveMin = new Values(true, false);
        // max abs negative values (far zero)
        Values negativeMax = new Values(false, true);
        // min abs negavite values (near zero)
        Values negativeMin = new Values(false, false);


        List<int> result = new List<int>();

        int count = int.Parse(NextToken());
        for (int i = 0; i < count; i++)
        {
            int number;
            if (int.TryParse(NextToken(), out number))
            {
                if (number >= 0)
                {   
                    if (number == 0)
                        zeroIndex = i;

                    positiveMax.TryAddValue(i, number);
                    positiveMin.TryAddValue(i, number);
                }
                else
                {
                    negativeMax.TryAddValue(i, number);
                    negativeMin.TryAddValue(i, number);
                }
            }
            else
                break;
        }

        Dictionary<int, int> indexToValue = new Dictionary<int, int>();

        Values[] values = new Values[] { positiveMax, positiveMin, negativeMax, negativeMin };

        for (int i = 0; i < values.Length; i++)
        {
            foreach (KeyValuePair<int, int> kvp in values[i].indexToValue)
            {
                if (!indexToValue.ContainsKey(kvp.Key))
                    indexToValue.Add(kvp.Key, kvp.Value);
            }
        }

        if (zeroIndex >= 0 && !indexToValue.ContainsKey(zeroIndex) && !indexToValue.ContainsValue(0))
        {
            indexToValue.Add(zeroIndex, 0);
        }

        List<int> numberValues = new List<int>();

        foreach (KeyValuePair<int, int> kvp in indexToValue)
            numberValues.Add(kvp.Value);

        result.AddRange(Multiply(numberValues));

        return result;
    }

    private List<int> Multiply(List<int> numbers)
    {
        List<int> result = new List<int>();

        int maxMultiplitacion = int.MinValue;

        for (int i = 0; i < numbers.Count; i++)
        {
            for (int j = 1; j < numbers.Count; j++)
            {
                for (int k = 2; k < numbers.Count; k++)
                {
                    if (i == j || j == k || k == i)
                        continue;

                    int multiplication = numbers[i] * numbers[j] * numbers[k];
                    if (multiplication > maxMultiplitacion)
                    {
                        maxMultiplitacion = multiplication;
                        result.Clear();
                        result.Add(numbers[i]);
                        result.Add(numbers[j]);
                        result.Add(numbers[k]);
                    }
                }
            }
        }

        return result;
    }

    class Values
    {
        public Dictionary<int, int> indexToValue = new Dictionary<int, int>();

        bool isPositive;
        bool isMax;

        public Values(bool isPositive, bool isMax)
        {
            this.isPositive = isPositive;
            this.isMax = isMax;
        }

        public bool TryAddValue(int index, int value)
        {
            indexToValue.Add(index, value);

            RemoveWorstElement(isPositive, isMax);

            return indexToValue.ContainsValue(value);
        }

        private void RemoveWorstElement(bool isPositive, bool isMax)
        {
            if (indexToValue.Count < 4)
                return;

            List<int> values = new List<int>();

            foreach (KeyValuePair<int, int> kvp in indexToValue)
            {
                values.Add(kvp.Value);
            }

            values.Sort();


            if (isPositive && isMax || !isPositive && !isMax) //�� �� �����, �� ����� ��������������
            {
                foreach (KeyValuePair<int, int> kvp in indexToValue)
                {
                    if (kvp.Value == values[0])
                    {
                        indexToValue.Remove(kvp.Key);
                        break;
                    }
                }
            }
            else if (isPositive && !isMax || !isPositive && isMax) //����������
            {
                foreach (KeyValuePair<int, int> kvp in indexToValue)
                {
                    if (kvp.Value == values[values.Count - 1])
                    {
                        indexToValue.Remove(kvp.Key);
                        break;
                    }
                }
            }
        }
    }
}