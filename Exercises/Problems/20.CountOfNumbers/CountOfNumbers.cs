using System;
using System.IO;
using System.Collections.Generic;
class CountOfNumbers
{
    #region ReadWrite
    List<int> output = new List<int>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        string outputValue = string.Empty;

        for (int i = 0; i < output.Count; i++)
        {
            outputValue += output[i] + " ";
        }

        sw.Write(outputValue.Trim());
        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        CountOfNumbers problem = new CountOfNumbers();
        problem.Start();
    }
    #endregion

    private List<int> ResolveProblem()
    {
        List<int> result = new List<int>();

        #region ������� 2
        int[] nums = new int[9];

        while (true)
        {
            int number = -1;

            if (!int.TryParse(NextToken(), out number))
                break;

            if (number == 0)
                break;

            nums[number - 1]++;
        }

        for (int i = 0; i < nums.Length; i++)
        {
            result.Add(nums[i]);
        }

        return result;
        #endregion

        #region ������� 1
        Dictionary<int, int> numbers = new Dictionary<int, int>();

        for (int i = 1; i < 10; i++)
        {
            numbers.Add(i, 0);
        }

        while (true)
        {
            int number = -1;

            if (!int.TryParse(NextToken(), out number))
                break;

            if (number == 0)
                break;

            numbers[number]++;
        }

        for (int i = 1; i < 10; i++)
        {
            result.Add(numbers[i]);
        }

        return result;
        #endregion
    }
}