using System;
using System.IO;
using System.Collections.Generic;
class Taxes
{
    #region ReadWrite
    List<int> output = new List<int>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        string outputValue = string.Empty;

        for (int i = 0; i < output.Count; i++)
        {
            outputValue += output[i] + " ";
        }

        sw.Write(outputValue.Trim());
        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        Taxes problem = new Taxes();
        problem.Start();
    }
    #endregion

    private List<int> ResolveProblem()
    {
        List<int> result = new List<int>();

        List<int> companiesIncome = new List<int>();

        int company = -1;
        int maxTaxValue = 0;

        int count = int.Parse(NextToken());

        for (int i = 0; i < count; i++)
        {
            int income = int.Parse(NextToken());
            companiesIncome.Add(income);
        }

        for (int i = 0; i < count; i++)
        {
            int tax = int.Parse(NextToken());
            int taxValue = companiesIncome[i] * tax; //������ �� 100 �� �����, ��� ���� float'�

            if (taxValue > maxTaxValue)
            {
                maxTaxValue = taxValue;
                company = i;
            }
        }

        result.Add(company + 1);

        return result;
    }
}