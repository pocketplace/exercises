using System;
using System.IO;
using System.Collections.Generic;
class SumOfDividers
{
    #region ReadWrite
    List<object> output = new List<object>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        string outputValue = string.Empty;

        for (int i = 0; i < output.Count; i++)
        {
            outputValue += output[i] + " ";
        }

        sw.Write(outputValue.Trim());
        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        SumOfDividers problem = new SumOfDividers();
        problem.Start();
    }
    #endregion

    private List<object> ResolveProblem()
    {
        List<object> result = new List<object>();

        int maxNumber = int.Parse(NextToken());

        int dividersSum = 1;
        int targetNumber = 1;

        for (int i = 1; i <= maxNumber; i++)
        {
            int tmpSum = i + 1;
            int maxDivider = (int)Math.Sqrt(i);

            for (int divider = 2; divider <= maxDivider; divider++)
            {
                if (i % divider == 0)
                {
                    tmpSum += divider;
                    if (divider * maxDivider != i)
                        tmpSum += i / divider;
                }
            }

            if (tmpSum > dividersSum)
            {
                dividersSum = tmpSum;
                targetNumber = i;
            }
        }

        result.Add(targetNumber);

        return result;
    }
}