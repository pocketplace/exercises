using System;
using System.IO;
using System.Collections.Generic;
class Minimum
{
    #region ReadWrite
    private int firstRow = 0;
    List<int> input = new List<int>();
    List<object> output = new List<object>();

    int lineIndex = 0;

    string inputFileName = "input.txt";
    string outputFileName = "output.txt";

    private void ReadFile(string fileName)
    {
        StreamReader sr = new StreamReader(fileName);

        string tmp;

        while (!string.IsNullOrEmpty(tmp = sr.ReadLine()))
        {
            TokenReadLine(tmp);
            ResolveProblem();
        }

        sr.Close();
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        for (int i = 0; i < output.Count; i++)
        {
            sw.WriteLine(output[i] + " ");
        }
        sw.Close();
    }

    private void TokenReadLine(string line)
    {
        string token = string.Empty;
        bool readToken = !char.IsWhiteSpace(line[0]);

        while (lineIndex < line.Length)
        {
            if (readToken)
            {
                if (char.IsWhiteSpace(line[lineIndex]))
                {
                    readToken = false;
                    input.Add(int.Parse(token));
                    token = string.Empty;
                }
                else
                {
                    token += line[lineIndex];
                }
            }
            else
            {
                if (!char.IsWhiteSpace(line[lineIndex]))
                {
                    readToken = true;
                    token += line[lineIndex];
                }
                else
                {
                    lineIndex++;
                    continue;
                }
            }

            lineIndex++;
        }

        lineIndex = 0;

        // �� �������� ��� ���������� �������� � ���� �������� ����� ������
        if (readToken)
        {
            input.Add(int.Parse(token));
        }
    }

    //private int[] ParseInput()
    //{
    //    int[] result = new int[input.Count];

    //    for (int i = 0; i < input.Count; i++)
    //    {
    //        int value;
    //        if (int.TryParse(input[i], out value))
    //            result[i] = value;
    //    }

    //    return result;
    //}
    #endregion

    #region Main
    public void Start()
    {
        ReadFile(inputFileName);

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        Minimum problem = new Minimum();
        problem.Start();
    }
    #endregion

    private void ResolveProblem()
    {
        int minimum = 101;
        for (int i = 0; i < input.Count; i++)
        {
            if (input[i] < minimum)
                minimum = input[i];
        }

        output.Add(minimum);
    }
}