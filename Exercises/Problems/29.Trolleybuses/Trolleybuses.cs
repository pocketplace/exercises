using System;
using System.IO;
using System.Collections.Generic;
class Trolleybuses
{
    #region ReadWrite
    List<int> output = new List<int>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        for (int i = 0; i < output.Count; i++)
        {
            sw.WriteLine(output[i]);
        }

        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        Trolleybuses problem = new Trolleybuses();
        problem.Start();
    }
    #endregion

    private List<int> ResolveProblem()
    {
        List<int> result = new List<int>();

        int period = int.Parse(NextToken());
        int count = int.Parse(NextToken());

        List<int> peopleArriveTimes = new List<int>(count);
        for (int i = 0; i < count; i++)
        {
            peopleArriveTimes.Add(int.Parse(NextToken()));
        }

        result.Add(ResolveFirstProblem(peopleArriveTimes, period));

        result.Add(ResolveSecondProblem(peopleArriveTimes, period));

        return result;
    }

    private int ResolveFirstProblem(List<int> times, int period)
    {
        int minTime = period * times.Count;
        int optimalStartTime = 0;

        for (int startTime = 0; startTime < period; startTime++)
        {
            int tmpMinTime = 0;
            for (int i = 0; i < times.Count; i++)
            {
                int peopleArriveTime = times[i];
                int trolleybusArriveTime = startTime;

                peopleArriveTime %= period;
                if (peopleArriveTime > trolleybusArriveTime)
                    trolleybusArriveTime += period;

                tmpMinTime += trolleybusArriveTime - peopleArriveTime;
            }

            if (tmpMinTime < minTime)
            {
                minTime = tmpMinTime;
                optimalStartTime = startTime;
            }
        }


        return optimalStartTime;
    }

    private int ResolveSecondProblem(List<int> times, int period)
    {
        int maxWaitingTime = period;
        int optimalStartTime = 0;

        for (int startTime = 0; startTime < period; startTime++)
        {
            int tmpMaxWaitingTime = 0;
            int maxWaitingTimeForThisStartTime = 0;

            for (int i = 0; i < times.Count; i++)
            {
                int peopleArriveTime = times[i];
                int trolleybusArriveTime = startTime;

                peopleArriveTime %= period;
                if (peopleArriveTime > trolleybusArriveTime)
                    trolleybusArriveTime += period;


                tmpMaxWaitingTime = trolleybusArriveTime - peopleArriveTime;

                if (tmpMaxWaitingTime > maxWaitingTimeForThisStartTime)
                    maxWaitingTimeForThisStartTime = tmpMaxWaitingTime;
            }

            if (maxWaitingTime > maxWaitingTimeForThisStartTime)
            {
                maxWaitingTime = maxWaitingTimeForThisStartTime;
                optimalStartTime = startTime;
            }
        }


        return optimalStartTime;
    }
}