using System;
using System.IO;
using System.Collections.Generic;
class Pairs
{
    #region ReadWrite
    List<object> output = new List<object>();

    const string inputFileName = "input.txt";
    const string outputFileName = "output.txt";

    string currentLine = string.Empty;
    int currentPos = 0;
    StreamReader sr = new StreamReader(inputFileName);

    private string NextToken()
    {
        while (true)
        {
            if (string.IsNullOrEmpty(currentLine) || currentPos >= currentLine.Length)
            {
                currentLine = sr.ReadLine();
                currentPos = 0;

                if (currentLine == null)
                    return null;
            }

            int startPos = -1;

            while (currentPos < currentLine.Length)
            {
                if (char.IsWhiteSpace(currentLine[currentPos]))
                {
                    currentPos++;
                    continue;
                }
                else
                {
                    if (startPos == -1)
                        startPos = currentPos;
                }

                if (currentPos + 1 >= currentLine.Length || char.IsWhiteSpace(currentLine[currentPos + 1]))
                {
                    string token = currentLine.Substring(startPos, currentPos - startPos + 1);
                    currentPos++;
                    return token;
                }

                currentPos++;
            }
        }
    }

    private void WriteToFile(string fileName)
    {
        StreamWriter sw = new StreamWriter(fileName);

        string outputValue = string.Empty;

        for (int i = 0; i < output.Count; i++)
        {
            outputValue += output[i] + " ";
        }

        sw.Write(outputValue.Trim());
        sw.Close();
    }
    #endregion

    #region Main
    public void Start()
    {
        output = ResolveProblem();

        WriteToFile(outputFileName);
    }

    static void Main()
    {
        Pairs problem = new Pairs();
        problem.Start();
    }
    #endregion

    private List<object> ResolveProblem()
    {
        List<object> result = new List<object>();
        int pairsCount = 0;

        Dictionary<int, int> valueToValueCount = new Dictionary<int, int>();

        int count = int.Parse(NextToken());

        for (int i = 0; i < count; i++)
        {
            int value = int.Parse(NextToken());

            if (valueToValueCount.ContainsKey(value))
            {
                valueToValueCount[value]++;
            }
            else
            {
                valueToValueCount.Add(value, 1);
            }
        }

        foreach (KeyValuePair<int, int> kvp in valueToValueCount)
        {
            pairsCount += PairsCount(kvp.Value);
        }

        result.Add(pairsCount);

        return result;
    }

    private int PairsCount(int value)
    {
        int result = 0;

        if (value == 1)
            return result;

        //    n!        (n-1) * n
        //___________ = _________
        //(n-2)! * 2!       2


        result = value * (value - 1) / 2;

        return result;
    }
}